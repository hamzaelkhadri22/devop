FROM openjdk:8-jdk-alpine
VOLUME /tmp
ARG JAR_FILE=target/*.jar
ADD target/devops-0.0.1-SNAPSHOT.jar devops-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/devops-0.0.1-SNAPSHOT.jar"]
